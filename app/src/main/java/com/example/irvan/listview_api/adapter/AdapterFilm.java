package com.example.irvan.listview_api.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.irvan.listview_api.R;
import com.example.irvan.listview_api.model.Film;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by irvan on 10/23/2016.
 */

public class AdapterFilm extends BaseAdapter {

    private List<Film.Data> film;
    private Context context;
    private LayoutInflater inflater;

    public AdapterFilm(Context context, List<Film.Data> film) {
        this.context = context;
        this.film = film;
        this.inflater = LayoutInflater.from(context);
    }

    public void addFilm(List<Film.Data> item){
        for (Film.Data listFilm : item){
            film.add(listFilm);
        }
    }

    @Override
    public int getCount() {
        return film.size();
    }

    @Override
    public Object getItem(int i) {
        return film.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder holder;

        if (view == null){
            holder = new ViewHolder();
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_film, null);

            holder.imgFilm = (ImageView)view.findViewById(R.id.img_film_detail);
            holder.judulFilm = (TextView)view.findViewById(R.id.txt_film_detail);
            holder.genreFilm = (TextView)view.findViewById(R.id.txt_genre_detail);
            holder.durationFilm = (TextView)view.findViewById(R.id.txt_duration_detail);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }

        Picasso.with(context).load(film.get(i).getPoster()).into(holder.imgFilm);
        holder.judulFilm.setText(film.get(i).getMovie());
        holder.genreFilm.setText(film.get(i).getGenre());
        holder.durationFilm.setText(film.get(i).getDuration());



        return view;
    }

    static class ViewHolder{
        ImageView imgFilm;
        TextView judulFilm, genreFilm, durationFilm;
    }
}

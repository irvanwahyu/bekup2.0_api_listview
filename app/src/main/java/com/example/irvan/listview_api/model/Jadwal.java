package com.example.irvan.listview_api.model;

import java.util.List;

/**
 * Created by irvan on 10/23/2016.
 */

public class Jadwal {
    private String bioskop;
    private List<Jam> jam;
    private String harga;

    public String getBioskop() {
        return bioskop;
    }

    public List<Jam> getJam() {
        return jam;
    }

    public String getHarga() {
        return harga;
    }

    public static class Jam {
        private String jam;

        public String getJam() {
            return jam;
        }
    }
}


package com.example.irvan.listview_api.model;

import java.util.List;

/**
 * Created by irvan on 10/21/2016.
 */

public class Film {

    private String status;
    private String kota;
    private String date;
    private List<Data> data;

    public String getStatus() {
        return status;
    }

    public String getKota() {
        return kota;
    }

    public String getDate() {
        return date;
    }

    public List<Data> getData() {
        return data;
    }

    public static class Data {
        private String movie;
        private String poster;
        private String genre;
        private String duration;
        //private List<Jadwal> jadwal;

        public String getMovie() {
            return movie;
        }

        public String getPoster() {
            return poster;
        }

        public String getGenre() {
            return genre;
        }

        public String getDuration() {
            return duration;
        }

//        public List<Jadwal> getJadwal() {
//            return jadwal;
//        }
    }
}

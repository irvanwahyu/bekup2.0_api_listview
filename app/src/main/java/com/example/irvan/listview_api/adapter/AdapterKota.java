package com.example.irvan.listview_api.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.irvan.listview_api.R;
import com.example.irvan.listview_api.model.Kota;

import java.util.List;

/**
 * Created by irvan on 10/21/2016.
 */

public class AdapterKota extends BaseAdapter {

    private List<Kota.Data> kota;
    private Context context;
    private LayoutInflater inflater;

    public AdapterKota(Context context, List<Kota.Data> kota) {
        this.kota = kota;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return kota.size();
    }

    @Override
    public Object getItem(int i) {
        return kota.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, final View convertView, ViewGroup viewGroup) {
        View view = convertView;
        ViewHolder holder;

        if (view == null){
            holder = new ViewHolder();
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_list, null);
            holder.txtKota = (TextView)view.findViewById(R.id.txt_kota);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }

        holder.txtKota.setText(kota.get(i).getKota());

        return view;
    }

    public static class ViewHolder{
        TextView txtKota;
    }
}

package com.example.irvan.listview_api;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.irvan.listview_api.adapter.AdapterKota;
import com.example.irvan.listview_api.model.Kota;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ListView lvItem;
    Kota kota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvItem = (ListView)findViewById(R.id.lv_film);

        API api = API.Factory.create();

        Call<Kota> listKota = api.getListKota();
        listKota.enqueue(new Callback<Kota>() {
            @Override
            public void onResponse(Call<Kota> call, Response<Kota> response) {
                AdapterKota adapter = new AdapterKota(MainActivity.this, response.body().getData());
                lvItem.setAdapter(adapter);

                kota = response.body();
                for (int i = 0; i < response.body().getData().size(); i++){
                    Log.d("Id   : "+i, kota.getData().get(i).getId());
                    Log.d("Kota : "+i, kota.getData().get(i).getKota());
                }
            }

            @Override
            public void onFailure(Call<Kota> call, Throwable t) {
                Log.d("TES", "GAGAL");
            }
        });


        lvItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra(DetailActivity.ITEM_ID, kota.getData().get(i).getId());
                intent.putExtra(DetailActivity.ITEM_KOTA, kota.getData().get(i).getKota());
                startActivity(intent);
            }
        });
    }
}

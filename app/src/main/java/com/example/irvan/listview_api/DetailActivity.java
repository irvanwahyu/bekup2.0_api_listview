package com.example.irvan.listview_api;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.irvan.listview_api.adapter.AdapterFilm;
import com.example.irvan.listview_api.model.Film;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    public static String ITEM_KOTA = "kota";
    public static String ITEM_ID = "id";
    private ListView lvFilm;
    private String detailKota, detailID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        detailKota = getIntent().getStringExtra(ITEM_KOTA);
        detailID = getIntent().getStringExtra(ITEM_ID);

        getSupportActionBar().setTitle(detailKota);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lvFilm = (ListView)findViewById(R.id.lv_detail);

        API api = API.Factory.create();

        Call<Film> listFilm = api.getListFilm(detailID);
        listFilm.enqueue(new Callback<Film>() {
            @Override
            public void onResponse(Call<Film> call, Response<Film> response) {
                AdapterFilm adapterFilm = new AdapterFilm(DetailActivity.this, response.body().getData());
                Log.d("TEST", String.valueOf(response.body().getData().size()));
                //adapterFilm.addFilm(response.body().getData());
                lvFilm.setAdapter(adapterFilm);
            }

            @Override
            public void onFailure(Call<Film> call, Throwable t) {
                Log.d("TES", "GAGAL "+t.getMessage());
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}

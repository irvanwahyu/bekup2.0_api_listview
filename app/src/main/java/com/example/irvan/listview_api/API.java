package com.example.irvan.listview_api;

import com.example.irvan.listview_api.model.Film;
import com.example.irvan.listview_api.model.Kota;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by irvan on 10/21/2016.
 */

public interface API {
    String baseURL = "http://ibacor.com/api/";

    @GET("jadwal-bioskop")
    Call<Kota> getListKota();

    @GET("jadwal-bioskop")
    Call<Film> getListFilm(@Query("id") String id);

    class Factory{
        public static API create(){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            return retrofit.create(API.class);
        }
    }

}
